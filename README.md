# Jira Stopwatch
> Awesome time tracking application made in electron and vue.js connected directly with JIRA API Rest.

## Features
>  * Multiplataform support
>  * Search of issues
>  * Native experience

## Releases
Current version - 1.0.6

#### Preview
##### Login screen

>![alt text](screenshots/login.PNG)

##### Main screen

>![alt text](screenshots/main.PNG)

#### Search issues screen

>![alt text](screenshots/search.PNG)

#### Settings screen

>![alt text](screenshots/settings.PNG)

#### Submit screen

>![alt text](screenshots/submit.PNG)


### Initialize project

``` bash
# install dependencies
npm install
```



### Run in development environment
``` bash
# serve with hot reload at localhost:9080
npm run dev

```
### Build  for production
``` bash

npm run build

```

> Developed by  Saul Martinez  -  saul.mtzs@gmail.com
