import Vue from 'vue'
import Electron from 'vue-electron'
import Resource from 'vue-resource'
import Router from 'vue-router'
import VueMask from 'v-mask'
const {remote} = require('electron')
const {Menu} = remote

import App from './App'
import routes from './routes'
window.jQuery = window.$ = require('jquery/dist/jquery.min')

Vue.use(Electron)
Vue.use(Resource)
Vue.use(Router)
Vue.use(VueMask)

Menu.setApplicationMenu(null)

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

/* eslint-disable no-new */
new Vue({
  router,
  ...App
}).$mount('#app')

$(document).ready(function () {
  $(window).resize(function () {
    let windowHeiht = $(window).height()
    $('.app-tasks').height(windowHeiht - 248)
    $('.task-grid').height(windowHeiht - 248)
    $('.app-tasks-search').height(windowHeiht - 150)
    $('.task-grid-search').height(windowHeiht - 150)
  })
})
