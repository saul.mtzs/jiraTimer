import * as types from '../mutation-types'

const state = {
  timer: {hours: 0, minutes: 0, description: '', key: '', project: '', successSubmit: false, removeAfter: ''},
  search: { key: '', id: 0 },
  new: {title: '', description: '', successNewTask: false},
  init: {id: 0}
}

const mutations = {
  [types.INSERT_VALUE] (state, value) {
    state.timer.hours = value.hours
    state.timer.minutes = value.minutes
    state.timer.id = value.id
    state.timer.key = value.key
    state.timer.description = value.description
    state.timer.project = value.project
  },
  [types.INSERT_NEW] (state, value) {
    state.new.title = value.title
    state.new.description = value.description
    state.new.successNewTask = true
  },
  [types.LOAD_VALUE] (state, value) {
    state.search.key = value.key
    state.search.id = value.id
  },
  [types.INIT_SEARCH] (state, value) {
    state.init.id = value
  },
  [types.CHANGE_STATUS] (state, value) {
    state.timer.successSubmit = value.status
    state.timer.removeAfter = value.remove
  },
  [types.CHANGE_STATUS_NEW] (state, value) {
    state.new.successNewTask = value
  },
  [types.CLEAR_INIT] (state, value) {
    state.init.id = 0
  },
  [types.CLEAR_SEARCH] (state, value) {
    state.search.key = ''
    state.search.id = 0
  },
  [types.CLEAR_NEW] (state, value) {
    state.new.title = ''
    state.new.description = ''
    state.new.successNewTask = false
  },
  [types.CLEAR_VALUES] (state, value) {
    state.timer.hours = 0
    state.timer.minutes = 0
    state.timer.description = ''
    state.timer.key = ''
    state.timer.project = ''
    state.timer.successSubmit = false
  }
}

export default {
  state,
  mutations
}
