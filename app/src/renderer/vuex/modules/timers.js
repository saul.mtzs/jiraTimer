import * as types from '../mutation-types'

const state = {
  timers: []
}

const mutations = {
  [types.INSERT_VALUE] (state, value) {
    state.timers.push()
  },
  [types.INCREMENT_MAIN_COUNTER] (state) {
    state.main++
  }
}

export default {
  state,
  mutations
}
