export default [
  {
    path: '/timer',
    name: 'timer-page',
    component: require('components/MainTimerPageView')
  },
  {
    path: '/',
    name: 'login-page',
    component: require('components/LoginPageView')
  },
  {
    path: '/new-task',
    name: 'new-task',
    component: require('components/NewTaskPageView.vue')
  },
  {
    path: '/settings',
    name: 'settings-page',
    component: require('components/SettingsPageView')
  },
  {
    path: '/submit-task',
    name: 'submit-task',
    component: require('components/TaskSubmitPageView')
  },
  {
    path: '/search-task',
    name: 'search-page',
    component: require('components/SearchTaskPage')
  },
  {
    path: '/profile',
    name: 'profile',
    component: require('components/ProfilePageView')
  },
  {
    path: '/history',
    name: 'history',
    component: require('components/HistoryPageView')
  },
  {
    path: '*',
    redirect: '/'
  }
]
