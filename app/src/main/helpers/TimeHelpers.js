/**
 * Created by Saul on 25/04/2017.
 */

export default {
  recordToJiraFormat (hours, minutes) {
    if (hours > 0) {
      if (minutes > 0) { return hours + 'h ' + minutes + 'm' } else { return hours + 'h' }
    }
    return minutes + 'm'
  },
  formatSearchQuery (assigned, project, type, status) {
    let query = ''
    if (assigned) {
      query += 'assignee=' + assigned.value
    }
    if (project) {
      if (!query) { query += 'project="' + project.value + '"' } else { query += ' AND project="' + project.value + '"' }
    }
    if (type) {
      if (!query) { query += 'issuetype="' + type.label + '"' } else { query += ' AND issuetype="' + type.label + '"' }
    }
    if (status) {
      if (!query) { query += 'status="' + status.label + '"' } else { query += ' AND status="' + status.label + '"' }
    }
    return query
  }
}

