'use strict'

import {ipcMain, app, BrowserWindow, Menu, MenuItem, shell} from 'electron'
import JIRA from './services/JiraService.js'
import TimeHelper from './helpers/TimeHelpers.js'

// uncomment next line for debug in production
// require('electron-debug')({showDevTools: true,enabled:true});

const Datastore = require('nedb')
const AutoLaunch = require('auto-launch')
const url = require('url')
const desktopIdle = require('desktop-idle');

const TO_DO_TASK_ID = 2;
const IN_PROGRESS_TASK_ID = 11;

const log = console.log
const winURL = process.env.NODE_ENV === 'development'
    ? `http://localhost:${require('../../../config').port}`
    : `file://${__dirname}/index.html`
const IDLE_STOP_VALUE_MILLISECONDS = 900000

let settingsStorage
let credentialsStorage
let timerStorage
let mainWindow
let systemIdleFlag = false

let path = null
if (global.process.platform === 'darwin') {
  path = global.process.execPath.split('.app/Content')[0] + '.app'
}
let appAutoLauncher = new AutoLaunch({
  name: app.getName(),
  path: path
})

function createWindow () {
    /**
     * Initial window options
     */

  mainWindow = new BrowserWindow({
    height: 560,
    minHeight: 560,
    minWidth: 400,
    maxWidth: 400,
    width: 400,
    resizable: true,
    title: '',
    show: false
  })

const isSecondInstance = app.makeSingleInstance((commandLine, workingDirectory) => {
    if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore()
        mainWindow.focus()
    }
})

if (isSecondInstance) {
    app.quit()
}

  mainWindow.loadURL(winURL)
  // mainWindow.webContents.openDevTools()

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  app.on('activate', () => {
    if (mainWindow === null) {
      createWindow()
    }
  })

  initProperties()

  setInterval(function () {
    if (desktopIdle.getIdleTime()> IDLE_STOP_VALUE_MILLISECONDS) {
      settingsStorage.findOne({isConfigured: true}, function (err, doc) {
        if (err) { return log(err) }
        if (doc !== null) {
          if (doc.pauseOnBlock === true) {
            systemIdleFlag = true
            mainWindow.webContents.send('pause-timer-due-idle')
          }
        }
      })
    } else if (systemIdleFlag) {
      systemIdleFlag = false
      mainWindow.webContents.send('resume-timer-due-idle')
    }
  }, 1000)
}

app.on('ready', () => {
  createWindow()
})

/*
 * creation of contextual menu items BEGIN
 */
const menu = new Menu()

menu.append(new MenuItem({ label: 'Start/Pause',
  click (menuItem, browserWindow, event) {
    browserWindow.webContents.send('context-item-start')
  }}))

menu.append(new MenuItem({ label: 'Edit time',
  click (menuItem, browserWindow, event) {
      browserWindow.webContents.send('context-item-edit')
  }}))

menu.append(new MenuItem({ label: 'Choose task',
  click (menuItem, browserWindow, event) {
      browserWindow.webContents.send('context-item-choose')
  }}))

menu.append(new MenuItem({ label: 'Delete',
  click (menuItem, browserWindow, event) {
    browserWindow.webContents.send('context-item-delete')
  }}))

menu.append(new MenuItem({ type: 'separator' }))

menu.append(new MenuItem({ label: 'Open in browser',
  click (menuItem, browserWindow, event) {
    browserWindow.webContents.send('context-item-external')
  }
}))

menu.append(new MenuItem({ label: 'Submit',
    click (menuItem, browserWindow, event) {
        browserWindow.webContents.send('context-item-submit')
    }}))

/*
 * creation of contextual menu items END
 */



/*
 * declaration of application events BEGIN
 */
ipcMain.on('erase-credentials', function (event) {
  eraseCredentials()
})

ipcMain.on('show-context-menu', function (event) {
  const mainWindow = BrowserWindow.fromWebContents(event.sender)
  menu.popup(mainWindow)
})

ipcMain.on('validate-hostname', (event, arg) => {
  validateHostname(event, arg)
})

ipcMain.on('init-profile', (event, arg) => {
  initProfile(event)
})

ipcMain.on('init-settings', (event, arg) => {
  initSettings(event)
})

ipcMain.on('init-login', (event, arg) => {
  initLogin(event)
})

ipcMain.on('save-settings', (event, arg) => {
  saveSettings(arg)
})

ipcMain.on('request-timers', (event, arg) => {
  getTimers(event, arg)
})

ipcMain.on('login', (event, arg) => {
  login(event, arg)
})

ipcMain.on('validate-issue', (event, arg) => {
  validateIssue(event, arg)
})

ipcMain.on('check-credentials', (event, arg) => {
  checkCredentials(event)
})

ipcMain.on('save-state', (event, arg) => {
  saveTimers(event, arg)
})

ipcMain.on('send-time', (event, arg) => {
  prepareTime(event, arg)
})

ipcMain.on('search-issues', (event, arg) => {
  searchIssues(event, arg)
})

ipcMain.on('initialize-search', (event, arg) => {
  initializeSearch(event, arg)
})

ipcMain.on('open-issue', (event, arg) => {
  openIssue(event, arg)
})

ipcMain.on('get-projects', (event, arg) => {
  getProjects(event, arg)
})

ipcMain.on('get-users', (event, arg) => {
  getUsers(event, arg)
})

ipcMain.on('get-types', (event, arg) => {
  getTypes(event, arg)
})

ipcMain.on('get-statuses', (event, arg) => {
  getStatuses(event, arg)
})


ipcMain.on('change-issue-status', (event, arg) => {
  changeIssueStatus(event, arg)
})

ipcMain.on('initialize-weakly-worklog', (event, arg) => {
  initializeWeaklyWorklog(event, arg)
})


ipcMain.on('get-status', (event, arg) => {
  getStatus(event,arg)
})

async function getStatus(event, arg){

  var issue = await getIssue(arg)
  var statuses =  await JIRA.getIssueTransitions(global.jiraUrl, global.auth, arg)
  
  if(issue == null)
  return
  var response = {status:issue.fields.status,statuses: statuses.data.transitions}

  event.sender.send('return-status',response)

}


/*
 * declaration of application events END
 */

async function initializeWeaklyWorklog(event, arg) {
  let response
  var date = new Date()
  var mondayDate = new Date(date.getTime() + ((24*60*60*1000)*(1- date.getDay())))
  var fridayDate = new Date(date.getTime() + (24*60*60*1000)*(5- date.getDay()))
  response = await JIRA.getWorklogs(global.jiraUrl,global.auth,getIsoDate(mondayDate),getIsoDate(fridayDate))  
  event.sender.send('initialize-weakly-worklog-success',response)
}

async function changeIssueStatus (key) {
  let issue = await getIssue(key);
  
  if(issue == null)
    return

  if(issue.fields.status.statusCategory.id == TO_DO_TASK_ID){
   let response = await JIRA.changeTaskStatus(global.jiraUrl, global.auth, key, IN_PROGRESS_TASK_ID)
   return response.status === 204 ? true : false;
    
  }
}


async function changeIssueStatusCustom (key,status) {
  let issue = await getIssue(key);
  
  if(issue == null)
    return
  log(status)
  let response = await JIRA.changeTaskStatus(global.jiraUrl, global.auth, key, status.value)
  log(response)
  return response.status === 204 ? true : false;
    
  
}

function eraseCredentials () {
  credentialsStorage.remove({}, { multi: true }, function (err, numRemoved) {
    if (err) { return log(err) }
  })
}

function compareKeyword (compareWord, element) {
  let name = String(element.name)
  let compare = String(compareWord)
  return name.toUpperCase().includes(compare.toUpperCase())
}

async function getUsers (event, arg) {
  let usersResponse
  usersResponse = await JIRA.getUsers(global.jiraUrl, global.auth, arg)
  if (usersResponse.status === 200) {
    event.sender.send('get-users-success', usersResponse.data)
  }
}
async function getTypes (event, arg) {
  let typesResponse
  typesResponse = await JIRA.getTypes(global.jiraUrl, global.auth, arg)
  if (typesResponse.status === 200) {
    event.sender.send('get-types-success', typesResponse.data)
  }
}
async function getStatuses (event, arg) {
  let statusesResponse
  statusesResponse = await JIRA.getStatuses(global.jiraUrl, global.auth, arg)
  if (statusesResponse.status === 200) {
    event.sender.send('get-statuses-success', statusesResponse.data)
  }
}
async function getProjects (event, arg) {
  let projetsResponse
  projetsResponse = await JIRA.getProjects(global.jiraUrl, global.auth, true)
  if (projetsResponse.status === 200) {
    let filteredProjects = projetsResponse.data.filter(compareKeyword.bind(this, arg))
    if (filteredProjects.length > 10) {
      filteredProjects = filteredProjects.slice(0, 10)
    }
    event.sender.send('get-projects-success', filteredProjects)
  }
}

function openIssue (event, arg) {
  shell.openExternal(url.format(url.parse(global.jiraUrl)) + 'browse/' + arg)
}

async function initializeSearch (event, arg) {
  let projetsResponse
  let usersResponse
  let typesResponse
  let statusesResponse
  let currentUser = global.username

  usersResponse = await JIRA.getUsers(global.jiraUrl, global.auth, currentUser)
  projetsResponse = await JIRA.getProjects(global.jiraUrl, global.auth, true)
  typesResponse = await JIRA.getTypes(global.jiraUrl,global.auth,'')
  statusesResponse = await JIRA.getStatuses(global.jiraUrl,global.auth,'')
  if (projetsResponse.status === 200 && usersResponse.status === 200) {
    let currentDisplayUser = usersResponse.data.find(x => x.name === currentUser)
    let filteredProjects = projetsResponse.data
    if (filteredProjects.length > 10) {
      filteredProjects = filteredProjects.slice(0, 10)
    }
    event.sender.send('initialize-search-success', { projects: filteredProjects, 
                                                     users: usersResponse.data, 
                                                     currentUser: currentDisplayUser, 
                                                     types: typesResponse.data, 
                                                     statuses: statusesResponse.data })
  } else {
    event.sender.send('initialize-search-failed')
  }
}

async function searchIssues (event, arg) {
  let jqlString = TimeHelper.formatSearchQuery(arg.user, arg.project, arg.type, arg.status)
  let response
  response = await JIRA.searchIssues(global.jiraUrl, global.auth, jqlString)
  
  if (response.status === 200) {
    event.sender.send('success-search', response.data)
    return
  } else {
    event.sender.send('failed-search', response)
  }
}

async function login (event, arg) {
  let response
  response = await JIRA.login(arg.hostname, arg.credentials.username, arg.credentials.password)
  if (response.status === 200) {
    global.username = arg.credentials.username
    credentialsStorage.findOne({haveCredentials: true}, function (err, doc) {
      if (err) { return log(err) }
      if (doc !== null) {
        if (doc.username !== arg.credentials.username || doc.password !== arg.credentials.password) {
          credentialsStorage.update({ haveCredentials: true }, {haveCredentials: true, username: arg.credentials.username, password: arg.credentials.password}, {}, function (err, numReplaced) {
            if (err) { return log(err) }
          })
        }
      } else {
        let credentials = {
          haveCredentials: true,
          username: arg.credentials.username,
          password: arg.credentials.password
        }
        credentialsStorage.insert(credentials, function (err, doc) {
          if (err) { return log(err) }
          if (doc === null) { return log('saving error') }
        })
      }
    })
    global.jiraUrl = arg.hostname
    settingsStorage.findOne({isConfigured: true}, function (err, doc) {
      if (err) { return log(err) }
      if (doc === null) {
        let settings = {'jiraUrl': arg.hostname, 'saveTimers': true, 'pauseOnBlock': false, 'startWithOS': false, 'isConfigured': true}
        settingsStorage.insert(settings, function (err, doc) {
          if (err) { return log(err) }
          if (doc === null) { return log('saving error') }
        })
      } else {
        settingsStorage.update({ isConfigured: true }, {'jiraUrl': arg.hostname, 'saveTimers': doc.saveTimers, 'pauseOnBlock': doc.pauseOnBlock, 'startWithOS': doc.startWithOS, 'isConfigured': true}, {}, function (err, numReplaced) {
          if (err) { return log(err) }
        })
      }
    })
    global.auth = Buffer.from(arg.credentials.username + ':' + arg.credentials.password).toString('base64')
    event.sender.send('login-success', response.data)
  } else {
    event.sender.send('login-failed', response)
  }
}

async function validateHostname (event, arg) {
  const success = await JIRA.validateHostname(arg)
  if (success) { event.sender.send('hostname-validation-success') } else { event.sender.send('hostname-validation-fail') }
}

async function prepareTime (event, arg) {
  let spendTime = TimeHelper.recordToJiraFormat(arg.hours, arg.minutes)
  let record = {
    key: arg.key,
    comment: arg.comment,
    spendTime: spendTime,
    remaining: arg.remaining,
    status: arg.status
  }
  validateStatusFlag(event, record)
}

async function validateIssue (event, arg) {
  let response
  response = await JIRA.searchForIssue(global.jiraUrl, global.auth, arg.key)
  if (response.status === 200) {
    event.sender.send('add-issue', {timerId: arg.timerId, data: response.data})
    return
  } else {
    event.sender.send('validate-issue-error', response)
  }
}



async function getIssue(key){
    let response
    response = await JIRA.searchForIssue(global.jiraUrl, global.auth, key)
    if (response.status === 200) 
      return response.data;
    return null;
}


function validateStatusFlag(event, record){
    settingsStorage.findOne({isConfigured: true}, function (err, doc)  {  
     if (err) log(err) 
     if(record.status !== null){
      changeIssueStatusCustom(record.key,record.status)
    }else if(doc !== null && doc.firstSubmitStatus){
       changeIssueStatus(record.key)
    }
    
     sendTime(event,record)
    })
}

async function sendTime(event, record){
  let response
  response = await JIRA.submitTime(global.jiraUrl, global.auth, record)
  if (response.status === 201) {
    event.sender.send('success-submit', response.data)
    return
  } else {
    event.sender.send('failed-submit', response)
  }
}


function saveTimers (event, arg) {
  settingsStorage.findOne({isConfigured: true}, function (err, doc) {
    if (err) { log(err) }
    if (doc !== null) {
      timerStorage.remove({}, { multi: true }, function (err, numRemoved) {
        if (err) { log(err) }
        if (doc.saveTimers === true) {
          arg.forEach(function (element) {
            delete element._id
          }, this)
          log(arg)
          timerStorage.insert(arg, function (err, doc) {
            if (err) { log(err) }
            if (doc === null) { return log('saving error') }
          })
        }
        event.returnValue = true
      })
    }
  })
}

function getTimers (event, arg) {
  timerStorage.find({}, function (err, docs) {
    if (err) { return log(err) }
    event.sender.send('restore-timers', docs)
  })
}

function checkCredentials (event) {
  if (global.jiraUrl) {
    credentialsStorage.findOne({haveCredentials: true}, function (err, doc) {
      if (err) { return log(err) }
      if (doc !== null) {
          let parameters = {
              hostname: global.jiraUrl,
              credentials: doc
          }
          global.username = parameters.credentials.username
          global.auth = Buffer.from(parameters.credentials.username + ':' + parameters.credentials.password).toString('base64')
          event.sender.send('bypass-login')
      }
    })
  }
}

function initProperties () {
  let userDataPath = app.getPath('userData')
  settingsStorage = new Datastore({ filename: userDataPath + `/db/settings.db`, autoload: true })
  credentialsStorage = new Datastore({filename: userDataPath + `/db/credentials.db`, autoload: true})
  timerStorage = new Datastore({filename: userDataPath + `/db/timers.db`, autoload: true})

  settingsStorage.findOne({isConfigured: true}, function (err, doc) {
    if (err) { return log(err) }
    if (doc !== null) {
      global.jiraUrl = doc.jiraUrl
    }
  })
}

function getIsoDate(date){
  return date.getFullYear()+ '-' + ('0' +(date.getMonth() + 1) ).slice(-2) + '-' + date.getDate()
}

function saveSettings (arg) {
  let settingsDoc = {
    jiraUrl: arg.jiraUrl,
    saveTimers: arg.saveTimers,
    pauseOnBlock: arg.pauseOnBlock,
    startWithOS: arg.startWithOS,
    firstSubmitStatus: arg.firstSubmitStatus,
    isConfigured: true
  }
  settingsStorage.findOne({isConfigured: true}, function (err, doc) {
    if (err) { return log(err) }
    if (doc !== null) {
      settingsStorage.update({ isConfigured: true }, {jiraUrl: arg.jiraUrl, saveTimers: arg.saveTimers, pauseOnBlock: arg.pauseOnBlock, startWithOS: arg.startWithOS, firstSubmitStatus: arg.firstSubmitStatus, isConfigured: true}, {}, function (err, numReplaced) {
        if (err) { return log(err) }
      })
    } else {
      settingsStorage.insert(settingsDoc, function (err, doc) {
        if (err) { return log(err) }
        if (doc === null) { return log('saving error') }
      })
    }
  })

  if (arg.startWithOS === true) {
    appAutoLauncher.isEnabled()
            .then(function (isEnabled) {
              if (isEnabled) {
                return
              }
              appAutoLauncher.enable()
            })
            .catch(function (err) {
              return log(err)
            })
  } else {
    appAutoLauncher.isEnabled()
            .then(function (isEnabled) {
              if (isEnabled) {
                appAutoLauncher.disable()
              }
            })
            .catch(function (err) {
              return log(err)
            })
  }
  global.jiraUrl = arg.jiraUrl
}

async function initProfile (event) {
  const response = await JIRA.profile(global.jiraUrl, global.auth)
  if (response.status === 200) { event.sender.send('initialize-profile', response.data) }
}

function initLogin (event) {
  settingsStorage.findOne({isConfigured: true}, function (err, doc) {
    if (err) { return log(err) }
    if (doc !== null) {
      event.sender.send('initialize-login', doc.jiraUrl)
    }
  })
}

function initSettings (event) {
  settingsStorage.findOne({isConfigured: true}, function (err, doc) {
    if (err) { return log(err) }
    if (doc !== null) {
      event.sender.send('initialize-settings', doc)
    }
  })
}
