/**
 * Created by Saul on 25/04/2017.
 */

import axios from 'axios'
const url = require('url')

function formatUrl (URL) {
  return url.format(url.parse(URL))
}

export default {
  async getProjects (JIRA_API_URL, AUTH, allProjets) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/project', {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async getUsers (JIRA_API_URL, AUTH, currentUser) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/user/search?username=' + currentUser, {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async getTypes (JIRA_API_URL, AUTH, arg) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/issuetype/' + arg, {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async getStatuses (JIRA_API_URL, AUTH, arg) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/status/' + arg, {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async searchIssues (JIRA_API_URL, AUTH, jqlString) {
    try {
      console.log(jqlString)
      return await axios.post(formatUrl(JIRA_API_URL) + 'rest/api/2/search', { jql: jqlString }, {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async validateHostname (JIRA_API_URL) {
    try {
      let response = await axios.get(formatUrl(JIRA_API_URL))
      if (response.status === 200) {
        return true
      }
    } catch (e) {
      return false
    }
  },
  async login (JIRA_API_URL, username, password) {
    try {
      console.log(formatUrl(JIRA_API_URL))
      return await axios.post(formatUrl(JIRA_API_URL) + 'rest/auth/1/session', {username: username, password: password})
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async profile (JIRA_API_URL, AUTH) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/myself', {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async searchForIssue (JIRA_API_URL, AUTH, issue) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/issue/' + issue, {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async submitTime (JIRA_API_URL, AUTH, record) {
    try {
      var adjustment = record.remaining ? '?adjustEstimate=new&newEstimate=0':''
      return await axios.post(formatUrl(JIRA_API_URL) + 'rest/api/2/issue/' + record.key + '/worklog' + adjustment,
      {
        timeSpent: record.spendTime,
        comment: record.comment
      }, 
      {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },

  async getWorklogs (JIRA_API_URL, AUTH, startDate, endDate) {
    try {
      var filters = encodeURI('worklogDate >= ' + startDate + ' AND worklogDate <= ' + endDate)
      var fields = "&fields=issuetype,fixVersions,resolution,created,priority,versions,status,components,summary,customfield_10201,customfield_10202,worklog,assignee";
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/search?jql=' + filters + fields,  {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async changeTaskStatus (JIRA_API_URL, AUTH, issue, transition) {
    try {
      return await axios.post(formatUrl(JIRA_API_URL) + 'rest/api/2/issue/' + issue + '/transitions', {transition: {id: transition}, update: {comment: [{add: {body: 'Status changed by timer'}}]}},
        {
          headers: {
            Authorization: 'Basic ' + AUTH
          }
        })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },
  async getIssueTransitions (JIRA_API_URL, AUTH, issue) {
    try {
      return await axios.get(formatUrl(JIRA_API_URL) + 'rest/api/2/issue/' + issue + '/transitions', {
        headers: {
          Authorization: 'Basic ' + AUTH
        }
      })
    } catch (e) {
      if (e.response) {
        return e.response
      }
      return e
    }
  },

}

